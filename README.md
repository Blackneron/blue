# Blue Template - README #
---

### Overview ###

**Blue** is a fast and responsive theme for the [**Bludit**](https://www.bludit.com/) CMS. The template is made out of plain PHP/HTML/CSS/JS. No jQuery, no Bootstrap, no other external framework, no heavy Google fonts or other bloat was used. Therefore the template is small and really fast to load. Well, it also depends on the number and size of the images used. There are three columns, the left one shows the last posts, the one in the middle the main content (page- or post content) and the right one shows an image gallery. There is a very small configuration file to specify some settings and a file with descriptions of the gallery images. The code is well documented with comments. To get a smaller HTML output, remove the comments on the live site. The template is responsive and should look great on any device. The used example images are from [**Streetwill**](http://streetwill.co/) and [**Pixabay**](https://pixabay.com/) and are licensed under [**CC0 - No Rights Reserved**](https://creativecommons.org/share-your-work/public-domain/cc0/).

### Screenshots ###

![Blue - Home - Desktop](development/readme/blue_1.jpg "Blue - Home - Desktop")
![Blue - Image View - Desktop](development/readme/blue_2.jpg "Blue - Image View - Desktop")
![Blue - Home - Tablet](development/readme/blue_3.jpg "Blue - Home - Tablet")
![Blue - Gallery - Tablet](development/readme/blue_4.jpg "Blue - Gallery - Tablet")
![Blue - Home - Mobile](development/readme/blue_5.jpg "Blue - Home - Mobile")
![Blue - Menu - Mobile](development/readme/blue_6.jpg "Blue - Menu - Mobile")
![Blue - Posts - Mobile](development/readme/blue_7.jpg "Blue - Posts - Mobile")
![Blue - Gallery - Mobile](development/readme/blue_8.jpg "Blue - Gallery - Mobile")
![Blue - Image View - Mobile](development/readme/blue_9.jpg "Blue - Image View - Mobile")

### Setup ###

* Copy the directory **blue** to the theme directory of your Bludit installation.
* Normally this is under **bludit/bl-themes/**.
* Open the file **bludit/bl-themes/blue/php/config.php** in your favorite text editor.
* Edit the view settings and save/upload the customized file.
* Upload your 'small' gallery images of the size 350 x 250 pixel to **blue/img/right/**.
* Upload your 'big' gallery images of the size 800 x 600 pixel or bigger to **blue/img/right/big/**.
* Customize also the image descriptions in the file **blue/img/right/description.txt**.
* Replace the header image **blue/img/altiplano.jpg** with your title image.
* Check the page in your browser and make modifications if necessary.
* Optional: Remove the comments from your template (especially from the file **blue/index.php**) if you want less HTML code!
* I would like to know if you are using this template, so write me a line to **biegel[at]gmx.ch**!
* Maybe you could also let the link in the footer :-) But hey, you can do whatever you want! No restrictions.

### Support ###

This is a free template and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Blue** template is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](http://opensource.org/licenses/MIT).
