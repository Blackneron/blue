<?php

// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                             Blue Theme                             //
//                                                                    //
//        A blue, fast and responsive theme for the Bludit CMS.       //
//                                                                    //
//                       For Bludit version 2.x                       //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 2.0 / 09.12.2018                      //
//                                                                    //
//                      Copyright 2018 - PB-Soft                      //
//                                                                    //
//                         https://pb-soft.com                        //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //

// Check that there is no direct script access.
if(!defined('BLUE') || !BLUE) {die();}

// Function to return the singular or plural reading time text.
function get_reading_time($reading_time_text) {

  // Get the page reading time.
  $reading_time = (int)$reading_time_text;

  // Check if the reading time is less or equal to one minute.
  if ($reading_time <= 1) {

    // Specify the page reading time text (singular minute).
    $reading_time_text = substr($reading_time_text, 0, -1);
  }

  // Return the reading time text.
  return $reading_time_text;
}

?>
